/*
 * Dark Web, a Web Extension to dark-theme the web.
 * Copyright (C) 2018  Aidan Gauland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const gulp = require("gulp");
const sass = require("gulp-sass");
const sassLint = require("gulp-sass-lint");
const autoprefixer = require("gulp-autoprefixer");
const eslint = require("gulp-eslint");
const del = require("del");

const paths = {
  styles: "styles/*.scss",
  js: "gulpfile.js"
};

gulp.task("clean", function() {
  return del(["build"]);
});

gulp.task("styles", function() {
  return gulp.src(paths.styles)
    .pipe(sass({outputStyle: "compressed"}).on("error", sass.logError))
    .pipe(autoprefixer({cascade: false}))
    .pipe(gulp.dest("build/styles"));
});

gulp.task("lint.styles", function () {
  return gulp.src(paths.styles)
    .pipe(sassLint())
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError());
});

gulp.task("lint.eslint", function() {
  // ESLint ignores files with "node_modules" paths.
  // So, it's best to have gulp ignore the directory as well.
  // Also, Be sure to return the stream from the task;
  // Otherwise, the task may end before the stream has finished.
  return gulp.src(paths.js)
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task("lint", ["lint.styles", "lint.eslint"]);

gulp.task("watch", ["styles"], function() {
  gulp.watch(paths.styles, ["styles"]);
});

gulp.task("default", ["styles"]);
